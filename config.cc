// Copyright (C) 2018 by bsdf

// This file is part of xtouchtool.

// xtouchtool is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// xtouchtool is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with xtouchtool.  If not, see <https://www.gnu.org/licenses/>.

#include <algorithm>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>

#include "config.hh"

xtt_config::xtt_config(std::string config_file)
{
  if (config_file.empty())
    {
      config_file = "~/.config/xtouchtool/xtt.json";
    }
  read_config(config_file);
}

std::vector<xtt_gesture>
xtt_config::gestures_from_json(std::vector<json11::Json> items)
{
  std::vector<xtt_gesture> gestures;
  std::transform(items.begin(), items.end(), std::back_inserter(gestures),
                 [](auto g)
                 {
                   auto type    = xtt_gesture::type_from_str(g["type"].string_value());
                   auto exec    = g["exec"].string_value();
                   auto enabled = g["enabled"].bool_value();

                   xtt_gesture xg(type, exec, enabled);
                   return xg;
                 });
  return gestures;
}

std::vector<xtt_app>
xtt_config::apps_from_json(std::vector<json11::Json> items)
{
  std::vector<xtt_app> apps;
  // std::transform(items.begin(), items.end(), std::back_inserter(apps),
  //                [](auto a)
  //                {
  //                });
  return apps;
}

void
xtt_config::read_config(std::string config_file)
{
  std::ifstream     ifs(config_file);
  std::stringstream ss;
  ss << ifs.rdbuf();

  std::string err;
  auto json = json11::Json::parse(ss.str(), err);

  if (!err.empty())
    {
      std::cerr << "there was an error parsing the config: "
                << err << std::endl;
      return;
    }

  auto global_json = json["global"];
  if (global_json.is_object())
    {
      global_gestures = gestures_from_json(global_json["gestures"].array_items());
    }
  
  auto apps_json = json["apps"];
  if (apps_json.is_array())
    {
      apps = apps_from_json(apps_json.array_items());
    }
}
