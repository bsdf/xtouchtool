// Copyright (C) 2018 by bsdf

// This file is part of xtouchtool.

// xtouchtool is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// xtouchtool is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with xtouchtool.  If not, see <https://www.gnu.org/licenses/>.

#include <iostream>

#include "xtouchtool.hh"
#include "controller.hh"
#include "config.hh"

int
main(int argc, char **argv)
{
  xtt_config config("../xtt.json");

  std::cout << "there are " << config.global_gestures.size() << " global gestures"
            << " " << config.global_gestures[0].exec << std::endl
            << "there are " << config.apps.size() << " apps" << std::endl;

  xtt_controller controller(config);
  return controller.start();
}
