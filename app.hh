// Copyright (C) 2018 by bsdf

// This file is part of xtouchtool.

// xtouchtool is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// xtouchtool is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with xtouchtool.  If not, see <https://www.gnu.org/licenses/>.

#include <map>
#include <string>
#include <vector>

#include "gesture.hh"
#include "selector.hh"

#ifndef XTT_APP_H
#define XTT_APP_H

class xtt_app
{
public:
  std::string name;

private:
  std::vector<xtt_selector> selectors;
  std::map<xtt_gesture::type, xtt_gesture> gestures;
};

#endif /* XTT_APP_H */
