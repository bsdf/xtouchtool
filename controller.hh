// Copyright (C) 2018 by bsdf

// This file is part of xtouchtool.

// xtouchtool is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// xtouchtool is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with xtouchtool.  If not, see <https://www.gnu.org/licenses/>.

#include <sys/fcntl.h>
#include <sys/select.h>
#include <unistd.h>

#include <libinput.h>
#include <libudev.h>

#include "config.hh"
#include "event_monitor.hh"

#ifndef XTT_CONTROLLER_H
#define XTT_CONTROLLER_H

const struct libinput_interface li_interface =
  {
   [](auto path, auto flags, auto data)
   {
     return open(path, flags);
   },
   [](auto fd, auto data)
   {
     close(fd);
   }
  };

class xtt_controller
{
public:
  xtt_controller(xtt_config config)
    : config {config} {};
  ~xtt_controller();

  int start();

private:
  struct udev     *udev;
  struct libinput *li;

  xtt_config config;

  void initialize_libinput();

  // event handlers
  void device_added_handler(li_event e);
  void swipe_started_handler(li_event e);
  void swipe_ended_handler(li_event e);
};

#endif /* XTT_CONTROLLER_H */
