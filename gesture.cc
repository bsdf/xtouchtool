// Copyright (C) 2018 by bsdf

// This file is part of xtouchtool.

// xtouchtool is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// xtouchtool is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with xtouchtool.  If not, see <https://www.gnu.org/licenses/>.

#include <map>

#include "gesture.hh"

std::map<std::string, xtt_gesture::type> type_map =
  {
   { "THREE_UP",    xtt_gesture::type::THREE_UP    },
   { "THREE_DOWN",  xtt_gesture::type::THREE_DOWN  },
   { "THREE_LEFT",  xtt_gesture::type::THREE_LEFT  },
   { "THREE_RIGHT", xtt_gesture::type::THREE_RIGHT },
   { "SCROLL_UP",   xtt_gesture::type::SCROLL_UP   },
   { "SCROLL_DOWN", xtt_gesture::type::SCROLL_DOWN },
   { "PINCH_IN",    xtt_gesture::type::PINCH_IN    },
   { "PINCH_OU",    xtt_gesture::type::PINCH_OUT   }
  };

xtt_gesture::type
xtt_gesture::type_from_str(std::string str)
{
  auto search = type_map.find(str);
  if (search == type_map.end())
    {
      // log warning..
      return type::NONE;
    }
  return search->second;
}

void
xtt_gesture::test()
{
  
}
