// Copyright (C) 2018 by bsdf

// This file is part of xtouchtool.

// xtouchtool is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// xtouchtool is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with xtouchtool.  If not, see <https://www.gnu.org/licenses/>.

#include <Xm/XmAll.h>

#include "ui.hh"

XtAppContext app;
Widget main_widget;

int
main(int argc, char **argv)
{
  short unsigned int xx, yy;
  main_widget = XtOpenApplication(&app, "My Application", NULL, 0, &argc, argv, NULL, applicationShellWidgetClass, NULL, 0);

  XtMakeResizeRequest(main_widget, 400, 100, &xx, &yy);

  XtRealizeWidget(main_widget);
  XtAppMainLoop(app);
  return 0;
}
