// Copyright (C) 2018 by bsdf

// This file is part of xtouchtool.

// xtouchtool is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// xtouchtool is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with xtouchtool.  If not, see <https://www.gnu.org/licenses/>.

#include <vector>

#include <json11.hpp>

#include "app.hh"
#include "gesture.hh"

#ifndef XTT_CONFIG_H
#define XTT_CONFIG_H

class xtt_config
{
public:
  std::vector<xtt_gesture> global_gestures;
  std::vector<xtt_app>     apps;

  xtt_config(std::string config_file);

private:
  void read_config(std::string config_file);
  std::vector<xtt_gesture> gestures_from_json(std::vector<json11::Json> items);
  std::vector<xtt_app> apps_from_json(std::vector<json11::Json> items);
};

#endif /* XTT_CONFIG_H */
