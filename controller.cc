// Copyright (C) 2018 by bsdf

// This file is part of xtouchtool.

// xtouchtool is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// xtouchtool is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with xtouchtool.  If not, see <https://www.gnu.org/licenses/>.

#include "event_monitor.hh"

#include "controller.hh"

void
xtt_controller::device_added_handler(li_event e)
{
  std::cout << "device added..." << std::endl;
}

void
xtt_controller::swipe_started_handler(li_event e)
{
  std::cout << "gesture started.." << std::endl;
}

void
xtt_controller::swipe_ended_handler(li_event e)
{
  std::cout << "gesture ended." << std::endl;
}

int
xtt_controller::start()
{
  initialize_libinput();
  return errno;
}

void
xtt_controller::initialize_libinput()
{
  udev = udev_new();
  li = libinput_udev_create_context(&li_interface, NULL, udev);

  if (libinput_udev_assign_seat(li, "seat0") != 0)
    {
      std::cerr << "failed to assign udev seat:" << std::endl;
      return;
    }

  xtt_event_monitor monitor(li);

  monitor.on(LIBINPUT_EVENT_DEVICE_ADDED,
             [=](auto e) { device_added_handler(e); });

  monitor.on(LIBINPUT_EVENT_GESTURE_SWIPE_BEGIN,
             [=](auto e) { swipe_started_handler(e); });

  monitor.on(LIBINPUT_EVENT_GESTURE_SWIPE_END,
             [=](auto e) { swipe_ended_handler(e); });

  monitor.start();
}

xtt_controller::~xtt_controller()
{
  udev_unref(udev);
  libinput_unref(li);
}
