// Copyright (C) 2018 by bsdf

// This file is part of xtouchtool.

// xtouchtool is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// xtouchtool is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with xtouchtool.  If not, see <https://www.gnu.org/licenses/>.

#include <libinput.h>

#include <functional>
#include <map>
#include <vector>
#include <iostream>
#include <algorithm>

#ifndef XTTEVENT_MONITOR_H
#define XTTEVENT_MONITOR_H

typedef struct libinput_event* li_event;
typedef std::function<void(li_event)> xtt_event_handler;

class xtt_event_monitor
{
public:
  xtt_event_monitor(struct libinput *i) : li{i}
  {
    libinput_ref(li);
  }

  ~xtt_event_monitor()
  {
    libinput_unref(li);
  }

  void on(libinput_event_type, xtt_event_handler);
  void start();

private:
  struct libinput *li;
  bool monitoring = true;
  std::map<libinput_event_type, std::vector<xtt_event_handler>> handlers;

  void read_events();
};

#endif /* XTTEVENT_MONITOR_H */
