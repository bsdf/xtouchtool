// Copyright (C) 2018 by bsdf

// This file is part of xtouchtool.

// xtouchtool is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// xtouchtool is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with xtouchtool.  If not, see <https://www.gnu.org/licenses/>.

#include "event_monitor.hh"

void
xtt_event_monitor::read_events()
{
  struct libinput_event *event;

  libinput_dispatch(li);
  while ((event = libinput_get_event(li)) != NULL)
    {
      auto type      = libinput_event_get_type(event);
      auto functions = handlers[type];

      std::for_each(functions.begin(), functions.end(),
                    [&](auto f) { f(event); });

      libinput_event_destroy(event);
    }
}

void
xtt_event_monitor::start()
{
  read_events(); // read initial events

  int fd = libinput_get_fd(li);

  fd_set set;
    
  FD_ZERO(&set);
  FD_SET(fd, &set);

  while (monitoring)
    {
      int selected = select(FD_SETSIZE, &set, NULL, NULL, NULL);
      switch (selected)
        {
        case 0:
          std::cout << "timed out..." << std::endl;
          break;

        case -1:
          std::cout << "err: " << errno << std::endl;
          break;

        default:
          read_events();
        }
    }
}

void
xtt_event_monitor::on(libinput_event_type type, xtt_event_handler handler)
{
  handlers[type].push_back(handler);
  std::cout << "handlers for this type: " << handlers[type].size() << std::endl;
}
