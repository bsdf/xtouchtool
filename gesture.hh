// Copyright (C) 2018 by bsdf

// This file is part of xtouchtool.

// xtouchtool is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// xtouchtool is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with xtouchtool.  If not, see <https://www.gnu.org/licenses/>.

#include <string>

#ifndef XTT_GESTURE_H
#define XTT_GESTURE_H

class xtt_gesture
{
public:
  enum class type
    {
     NONE,

     THREE_UP,
     THREE_DOWN,
     THREE_LEFT,
     THREE_RIGHT,

     SCROLL_UP,
     SCROLL_DOWN,

     PINCH_IN,
     PINCH_OUT
    };

  static xtt_gesture::type type_from_str(std::string);

  xtt_gesture::type t;
  std::string exec;
  bool enabled;

  xtt_gesture(xtt_gesture::type t, std::string exec, bool enabled)
    : t {t}, exec {exec}, enabled {enabled} {}


private:
  void test();
};

#endif /* XTT_GESTURE_H */
